<?php
include_once("Persona.php");
include_once("serializarArchivo.php");

if(isset($_GET['id']))
{
    $idPersona = $_GET['id'];

    $listarPersonas = serializarArchivo::deserializar();

    for($i = 0; $i < count($listarPersonas); $i++)
    {
        if($listarPersonas[$i]->id == $idPersona)
        {
            $personaActual = $listarPersonas[$i];
            break;
        }
    }

    ?>

    <form method="POST" action="actualizarPersona.php">
        <p>Id: <input type="number" name="id" <?php echo "readonly" ?> value="<?php echo $personaActual->id; ?>" /></p>
        <p>Nombre: <input type="text" name="nombre" value="<?php echo $personaActual->nombre; ?>" /></p>
        <p>Edad: <input type="number" name="edad" value="<?php echo $personaActual->edad; ?>" /></p>
        <p>Sexo: <select name="sexo">
                    <option value="Masculino" <?php if($personaActual->sexo == "Masculino") echo "selected"; ?>>
                        Masculino
                    </option>
                    <option value="Femenino" <?php if($personaActual->sexo == "Femenino") echo "selected"; ?>>
                        Femenino
                    </option>
                </select></p>
        <p><input type="submit" name="actualizarPersona" value="Guardar" /></p>
    </form>

    <?php

}

?>
<br>
<a href="listarPersonas.php">Regresar</a>