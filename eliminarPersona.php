<?php

include_once("Persona.php");
include_once("serializarArchivo.php");

if(isset($_POST['eliminarPersona']))
{
    $listaPersonas = serializarArchivo::deserializar();

    for($i = 0; $i < count($listaPersonas); $i++)
    {
        if($listaPersonas[$i]->id == $_POST['id'])
        {
            unset($listaPersonas[$i]);
            $listaPersonas = array_values($listaPersonas);
            break;
        }
    }

    $resultado = serializarArchivo::serializar($listaPersonas);

    if($resultado)
        echo "<p>Dato borrado correctamente</p>";
    else
        echo "<p>Error al borrar el dato</p>";
}
?>

<a href="listarPersonas.php">Regresar</a>