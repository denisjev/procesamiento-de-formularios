<?php
include_once("Persona.php");
include_once("serializarArchivo.php");

if(isset($_GET['id']))
{
    $idPersona = $_GET['id'];

    $listarPersonas = serializarArchivo::deserializar();

    for($i = 0; $i < count($listarPersonas); $i++)
    {
        if($listarPersonas[$i]->id == $idPersona)
        {
            $personaActual = $listarPersonas[$i];
            break;
        }
    }

    ?>

    <form method="POST" action="eliminarPersona.php">
        <input type="hidden" name="id" value="<?php echo $personaActual->id; ?>" />
        <p>Nombre: <?php echo $personaActual->nombre; ?></p>
        <p>Edad: <?php echo $personaActual->edad; ?></p>
        <p>Sexo: <?php echo $personaActual->sexo; ?></p>
        <br>
        <p>¿Seguro que desea eliminar el registro?</p>
        <p><input type="submit" name="eliminarPersona" value="Eliminar" /></p>
    </form>


<?php
}
?>
<br>
<a href="listarPersonas.php">Regresar</a>