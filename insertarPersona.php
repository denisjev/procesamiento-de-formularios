<?php

include_once("Persona.php");
include_once("serializarArchivo.php");

if(isset($_POST['agregarPersona']))
{
    $listaPersonas = serializarArchivo::deserializar();

    $nuevoPersona = new Persona((int)$_POST['id'], $_POST['nombre'], $_POST['edad'], $_POST['sexo']);

    array_push($listaPersonas, $nuevoPersona);

    $resultado = serializarArchivo::serializar($listaPersonas);

    if($resultado)
        echo "<p>Dato insertado correctamente</p>";
    else
        echo "<p>Error al insertar el dato</p>";
}
?>

<img src="<?php echo $p->nombreImagen; ?>" />

<a href="index.php">Regresar</a>