<?php

include_once("Persona.php");
include_once("serializarArchivo.php");

$listarPersonas = serializarArchivo::deserializar();

?>

<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">

    <title>Hello, world!</title>
  </head>
  <body>
    <h1>Hola mundo</h1>

    <table class="table table-striped">
        <thead class="thead-dark">
            <tr><th>Id</th><th>Nombre</th><th>Edad</th><th>Sexo</th><th colspan="2">Acciones</th></tr>
        </thead>

    <?php
        foreach($listarPersonas as $persona)
        {
            echo "<tr><td>" . $persona->id . "</td>" . 
            "<td>" . $persona->nombre . "</td>" . 
            "<td>" . $persona->edad . "</td>" . 
            "<td>" . $persona->sexo . "</td>" . 
            "<td class='btn btn-warning'> <a href='editarPersona.php?id=" . $persona->id . "'>Editar</a>" . 
            "<td> <a href='borrarPersona.php?id=" . $persona->id . "'>Borrar</a>" . 
            "</td></tr>";
        }
    ?>

    </table>

    <br />
    <a class="button" href="index.php">Regresar</a>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="js/jquery-3.3.1.slim.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>

