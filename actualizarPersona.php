<?php

include_once("Persona.php");
include_once("serializarArchivo.php");

if(isset($_POST['actualizarPersona']))
{
    $listaPersonas = serializarArchivo::deserializar();

    for($i = 0; $i < count($listaPersonas); $i++)
    {
        if($listaPersonas[$i]->id == $_POST['id'])
        {
            $listaPersonas[$i]->nombre = $_POST['nombre'];
            $listaPersonas[$i]->edad = $_POST['edad'];
            $listaPersonas[$i]->sexo = $_POST['sexo'];
            break;
        }
    }

    $resultado = serializarArchivo::serializar($listaPersonas);

    if($resultado)
        echo "<p>Dato actualizado correctamente</p>";
    else
        echo "<p>Error al actualizar el dato</p>";
}
?>

<a href="listarPersonas.php">Regresar</a>